// Add script(src='http://localhost:35729/livereload.js') on layout
// <!-- To be deleted after development finishes -->
'use strict';

module.exports = function (grunt) {
  // show elapsed time at the end
  require('time-grunt')(grunt);

  require('load-grunt-tasks')(grunt);

  var reloadPort = 35729;

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    watch: {
      options: {
        nospawn: true,
        livereload: reloadPort
      },
      js: {
        files: ['js/**/*.js','js/*.js', 'Gruntfile.js'],
        options: {
          livereload: reloadPort
        }
      },
      html: {
        files: ['index.html','partials/*.html','templetes/*.html'],
        options: {
          livereload: reloadPort
        }
      },
      css: {
        files: ['css/*.css'],
        options: {
          livereload: reloadPort
        }
      }
    }
  });

  grunt.registerTask('delayed-livereload', 'Live reload after the node server has restarted.', function () {
    var done = this.async();
    setTimeout(function () {
      request.get('http://localhost:' + reloadPort + '/changed?files=' + files.join(','), function (err, res) {
        var reloaded = !err && res.statusCode === 200;
        if (reloaded) {
          grunt.log.ok('Delayed live reload successful.');
        } else {
          grunt.log.error('Unable to make a delayed live reload.');
        }
        done(reloaded);
      });
    }, 500);
  });

  grunt.registerTask('default', [
    'watch'
  ]);
};
