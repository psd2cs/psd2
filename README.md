# Install app

```shell
> git clone https://avallverdu@bitbucket.org/avallverdu/corp-bank-angular-app.git yourPreferredName
> cd yourPreferredName
# npm is only used for development purpose
> npm install
```


# Run app

```shell
# run local web server to avoid CORS problems
> http-server
# open another terminal window and run grunt to reload the browser when a file is changed
> grunt
```

Visit [http:127.0.0.1:8080](http:127.0.0.1:8080)

**The enabled global user is "999", there is no need to type a password.**

### Old Instructions

  1. CSS error:
    - 1) There are 8 warnings on screen.css.
Please ignore them,they are cause by the prefix attribute for the browsers.
    - 2) Please ignore the errors & warnings on angular-bootstrap-calendar.css & bootstrap.css
The angular-bootstrap-calendar.css is used to style the calendar event layout & the bootstrap.css is style of bootstrap.

  2. JS library:
    * jquery-2.1.4.js ------ Jquery library.
    * angular.js
    * angular-animate.js
    * angular-messages.js
    * angular-route.js
    * angular-sanitize.js ------ Angular base library.
    * angular-backtop.js ------ Back To Top library that use for the 'Back To Top' button.
    * angular-bootstrap-calendar-tpls.js
    * moment.min.js ------ Calendar event library.
    * angular-dropdowns.js ------ custom dropdown library.
    * ng-flow.js
    * ng-flow-standalone.js ------ File Upload library.
    * ui-bootstrap-tpls-1.2.4.js ------ Angular for bootstrap library.

  3. Login Access:
   * username: user
   * password: 123456
