/// <summary>
/// This is the trade Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

"use strict";

(function () {

    // documents service
    var documentsService = function ($http,$log,common, CONFIG) {
        // private functions

        // get all the documents for a client
        var getAllDocumentsForClient = function (clientId) {
            return $http({
                method: 'GET',
                url: CONFIG.restApiEndPoint + '/documents/' + clientId
            });
        };

        // create a new document for a client
        var createNewDocumentForClient = function (clientId, document) {
            // Get data
            return $http({
                method: 'POST',
                url: CONFIG.restApiEndPoint + '/documents/' + clientId,
                data: document
            });
        };

        // public functions
        return {
            getAllDocumentsForClient: getAllDocumentsForClient,
            createNewDocumentForClient: createNewDocumentForClient
        }
    };

    // declaration of the service
    var declaration = [
        '$http',
         '$log',
        'common',
        'CONFIG',
        documentsService
    ];

    // Create the service
    angular.module("dinnaco.services").factory("documentsService", declaration);

})();