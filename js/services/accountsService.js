/// <summary>
/// This is the trade Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

"use strict";

(function () {

    // accounts service
    var accountsService = function ($http,$log,common, CONFIG) {
        // private functions

        // Get all the accounts for a client
        var getAccountsForClient = function (clientId) {
            // Get data
            return $http({
                method:'GET',
                url: CONFIG.restApiEndPoint + '/accounts/' + clientId
            });
        };

        // public functions
        return {
            getAccountsForClient: getAccountsForClient
        }
    };

    // declaration of the service
    var declaration = [
        '$http',
         '$log',
        'common',
        'CONFIG',
        accountsService
    ];

    // Create the service
    angular.module("dinnaco.services").factory("accountsService", declaration);

})();
