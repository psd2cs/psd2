/// <summary>
/// This is the trade Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

"use strict";

(function () {

    // clients service
    var clientsService = function ($http, $log, common, CONFIG) {
        // private functions

        // Get all the clients
        var getAllClients = function () {
            return $http({
                method: 'GET',
                url: CONFIG.restApiEndPoint + '/clients'
            });
        };

        // Get a client
        var getClient = function (clientId) {
            return $http({
                method: 'GET',
                url: CONFIG.restApiEndPoint + '/clients/' + clientId
            });
        };

        // public functions
        return {
            getAllClients: getAllClients,
            getClient: getClient
        }
    };

    // declaration of the service
    var declaration = [
        '$http',
         '$log',
        'common',
        'CONFIG',
        clientsService
    ];

    // Create the service
    angular.module("dinnaco.services").factory("clientsService", declaration);

})();