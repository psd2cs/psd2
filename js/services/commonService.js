/// <summary>
/// This is the trade Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

"use strict";

(function () {

    // common services
    var commonService = function ($http, $q) {
        // private functions
        var makeRequest = function (options) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: options.url
            }).
			success(function (data, status, headers, config) {
			    deferred.resolve(data);
			}).
			error(function (data, status, headers, config) {
			    deferred.reject(data);
			});
            return deferred.promise;
        };

        // Gets the symbol of the given currency code
        var getCurrencySymbol = function (currency) {
            if (currency === "EUR") return '\u20AC';
            if (currency === "USD") return '\u0024';
            if (currency === "GBP") return '\u00A3';
            return '\u00A3';
        }

        // Add the currency symbol to a balance, depending on which currency is given
        var addCurrencySymbolToBalance = function (balance, currency) {
            if (currency === "EUR") {
                // �
                return getCurrencySymbol("EUR") + " " + balance;
            } else if (currency === "USD") {
                // $
                return getCurrencySymbol("USD") + " " + balance;
            } else if (currency === "GBP") {
                // �
                return getCurrencySymbol("GBP") + " " + balance;
            }
            // Default to � if no currency is provided
            return getCurrencySymbol("GBP") + " " + balance;
        }

        // shortens an amount with 1000 = k  and 1 000 000 = m
        // and adds the currency symbol to it.
        var transformBalanceToHighDigitNumberWithCurrencySymbol = function (balance, currency) {
            // The thousands indicator ( "", "k", or "m")
            var thousandsIndicator = "";
            // the resulting balance
            var newBalance = balance;
            // flag to check negative values
            var isNegative = false;

            if (Math.sign(newBalance) === -1) {
              newBalance = Math.abs(newBalance);
              isNegative = true;
            }

            // if newBalance > 1 000 000 then divide the newBalance by 1 000 000 and set thousands indicator to m
            if (newBalance > 1000000) {
                thousandsIndicator = "m";
                newBalance = newBalance / 1000000;
            // if newBalance > 1 000 then divide the newBalance by 1 000 and set thousands indicator to k
          } else if (newBalance > 1000) {
                thousandsIndicator = "k";
                newBalance = newBalance / 1000;
            }

            // round balance to 1 digit after comma
            newBalance = Math.round(newBalance * 10) / 10;

            if(isNegative) {
              newBalance = -Math.abs(newBalance);
            }

            // add currency symbol
            switch (currency) {
              case "EUR":
                return getCurrencySymbol("EUR") + " " + newBalance + thousandsIndicator;
              case "USD":
                return getCurrencySymbol("USD") + " " + newBalance + thousandsIndicator;
              default:
                return getCurrencySymbol("GBP") + " " + newBalance  +  thousandsIndicator;
            }
        };

        // Custom object to save for the bank summary panel
        var buildObjectBankSummary = function(array) {
          var totalCount = 0;
          var bankObject = {};

          array.forEach(element => {
            totalCount += Number(element.balance);
            bankObject.bankName = element.bankName;
            bankObject.bankImage = element.bankImage;
            bankObject.count = transformBalanceToHighDigitNumberWithCurrencySymbol(totalCount, "GBP");;
          });

          return bankObject;
        };

        // Custom object to save for the current accounts panel
        var buildObjectAccountCurrentAssets = function(array) {
          var totalCount = 0;
          var accountObject = {};

          array.forEach(element => {
            totalCount = Number(element.balance);
            accountObject.name = element.idModified;
            accountObject.bankImage = element.bankImage;
            accountObject.amount = addCurrencySymbolToBalance(addThousandsSeparator(totalCount, ","), "GBP");
          });

          return accountObject;
        }

        // returns the amount with the given thousand separator
        var addThousandsSeparator = function (amount, separator) {
            amount += '';
            var x = amount.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + separator + '$2');
            }
            return x1 + x2;
        };

        // property to indicate whether or not user is in the trade section of the app
        var isInTradesSection;

        // public functions
        return {
            makeRequest: makeRequest,
            getIsInTradesSection: function () {
                return isInTradesSection;
            },
            setIsInTradesSection: function (value) {
                isInTradesSection = value;
            },
            addCurrencySymbolToBalance: addCurrencySymbolToBalance,
            addThousandsSeparator: addThousandsSeparator,
            transformBalanceToHighDigitNumberWithCurrencySymbol: transformBalanceToHighDigitNumberWithCurrencySymbol,
            buildObjectBankSummary: buildObjectBankSummary,
            buildObjectAccountCurrentAssets: buildObjectAccountCurrentAssets
        }
    };


    // Create the service
    angular.module("dinnaco.services").factory("common", commonService);

})();
