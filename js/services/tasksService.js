/// <summary>
/// This is the trade Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

"use strict";

(function () {

    // tasks service
    var tasksService = function ($http, $log, common, CONFIG) {
        // private functions

        // Get all tasks for the client
        var getAllTasksForClient = function (clientId) {
            return $http({
                method: 'GET',
                url: CONFIG.restApiEndPoint + '/tasks/' + clientId
            });
        };

        // Create new task for the client
        var createNewTaskForClient = function (clientId, task) {
            var req = {
                method: 'POST',
                url: CONFIG.restApiEndPoint + '/tasks/' + clientId,
                headers: {
                },
                data: task
            }

            return $http(req);
        };

        // filter the given tasks so only "trading" tasks are returned
        var getTradeTasks = function (tasks) {
            var tradeTasks = [];
            angular.forEach(tasks, function (value) {
                if (value.type === "Export Letter of Credit" || value.type === "Import Letter of Credit" || value.type === "Guarantee"
                    || value.type === "Import Collection" || value.type === "Export Collection" || value.type === "Invoice Financing"
                    || value.type === "Supply Chain Financing" || value.type === "Pre-shipment Financing") {
                    this.push(value);
                }
            }, tradeTasks);
            return tradeTasks.reverse();
        };

        // public
        return {
            getAllTasksForClient: getAllTasksForClient,
            createNewTaskForClient: createNewTaskForClient,
            getTradeTasks: getTradeTasks
        }
    };

    // declaration of the service
    var declaration = [
        '$http',
         '$log',
        'common',
        'CONFIG',
        tasksService
    ];

    // Create the service
    angular.module("dinnaco.services").factory("tasksService", declaration);

})();
