/// <summary>
/// This is the recommended Actions Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

'use strict';

// self invoking function
(function () {

    // implementation of the recommended Actions Controller
    var recommendedActionsController = function ($scope, common) {

        // logout clicked
        $scope.logout = function () {
            $scope.mainLogout();
        };

        // check if user is logged in
        $scope.checkForLoggedIn();

        //Get data for panel of 'Recommended Actions'
        var promise = common.makeRequest({
            method: 'GET',
            url: 'data/recommended_actions.json'
        });
        promise.then(function (data) {
            $scope.dataSet = {};
            $scope.dataSet['Recommended Actions'] = data.dataSet['Recommended Actions'];
        });
    };

    // declaration of the controller
    var declaration = [
        '$scope',
        'common',
        recommendedActionsController
    ];

    // Create the controller
    angular.module("dinnaco").controller("recommendedActionsController", declaration);

})();
