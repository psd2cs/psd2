/// <summary>
/// This is the summary Account Infomration Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

'use strict';

// self invoking function
(function () {

    // implementation of the summary Account Infomration Controller
    var summaryAccountInfomrationController = function ($scope, common, accountsService) {

        // logout clicked
        $scope.logout = function () {
            $scope.mainLogout();
        };

        // check if user is logged in
        $scope.checkForLoggedIn();

        // get id of currently logged in user
        var clientId = $scope.getClientId();
        console.log(clientId,'clientId clientId');

        //Get data for panel of 'Summary Account Information'
        var promise = common.makeRequest({
            method: 'GET',
            url: 'data/summary_account_information.json'
        });
        promise.then(function (data) {
            $scope.dataSet = {};
            $scope.dataSet['Summary Account Information'] = data.dataSet['Summary Account Information'];

            //Get data for panel of 'Summary Account Information'
            // Get all accounts of the client
            accountsService.getAccountsForClient(clientId).then(function (response) {

              // The deposit accounts total count
              var depositBalance = 0;
              // The loan accounts total count
              var loanBalance = 0;
              // Group for all the deposit accounts
              var depositGroup;
              // Group for all the loan accounts
              var loanGroup;
              // bank containers
              var sampsonBankDepositContainer;
              var sampsonBankLoanContainer;
              var titanBankDepositContainer;
              var titanBankLoanContainer;

              // Retrieving the actual data structure so we can set some of the properties
              var depositDataSetAccounts = $scope.dataSet['Summary Account Information']["DEPOSIT"]["accounts"];
              var loanDataSetAccounts = $scope.dataSet['Summary Account Information']["LOAN"]["accounts"];

              // Temp arrays to split accounts into loan and deposit types and
              // modify each entry of array so it'll hold an Account object
              var depositContainer = response.data.filter( account => account.accountType === 'DEPOSIT')
                .map(depositAccount => {
                    depositAccount = new Account(depositAccount.id, depositAccount.productName, depositAccount.balance, depositAccount.accountType, depositAccount.notes);
                    return depositAccount;
                });

              var loanContainer = response.data.filter( account => account.accountType === 'LOAN')
                .map(loanAccount => {
                    loanAccount = new Account(loanAccount.id, loanAccount.productName, loanAccount.balance, loanAccount.accountType, loanAccount.notes);
                    return loanAccount;
                });

              console.log(depositContainer, 'depositContainer');
              console.log(loanContainer, 'loanContainer');

              // Calculate total count for all deposit accounts
              depositContainer.forEach(depositAccount => {
                depositBalance += Number(depositAccount.balance);
                return depositBalance;
              });

              // Calculate total count for all loan accounts
              loanContainer.forEach(loanAccount => {
                loanBalance += Number(loanAccount.balance);
                return loanBalance;
              });

              // Setting new totalCount values for deposit and loan accounts
              $scope.dataSet['Summary Account Information']["DEPOSIT"].totalCount = common.transformBalanceToHighDigitNumberWithCurrencySymbol(depositBalance, "GBP");
              $scope.dataSet['Summary Account Information']["LOAN"].totalCount = common.transformBalanceToHighDigitNumberWithCurrencySymbol(loanBalance, "GBP");

              console.log(depositBalance, 'depositBalance');
              console.log(loanBalance, 'loanBalance');

              // Grouping deposit accounts by name property
              depositGroup = _.groupBy(depositContainer, 'name');
              // // Grouping loan accounts by name property
              loanGroup = _.groupBy(loanContainer, 'name');

              // Getting total sum for different types of deposit accounts
              _.each( depositGroup, (containerTypeAccount) => {
                _.reduce( containerTypeAccount, (result, value, key) => {
                  var depositCount = _.find(depositDataSetAccounts, {name: value.name});
                  depositCount.count +=  Number(value.balance);
                }, 0);
              });

              // Getting total sum for different types of loan accounts
              _.each( loanGroup, (containerTypeAccount) => {
                _.reduce( containerTypeAccount, (result, value, key) => {
                  var loanCount = _.find(loanDataSetAccounts, {name: value.name});
                  loanCount.count +=  Number(value.balance);
                }, 0);
              });

              // Formatting the Account's total values to a more readable string
              depositDataSetAccounts.forEach(account => {
                account.count = common.transformBalanceToHighDigitNumberWithCurrencySymbol(account.count, "GBP");
              });
              loanDataSetAccounts.forEach(account => {
                account.count = common.transformBalanceToHighDigitNumberWithCurrencySymbol(account.count, "GBP");
              });


              // Getting data for banks section
              sampsonBankDepositContainer = depositContainer.filter(bank => bank.bankName === "Sampson Bank");
              titanBankDepositContainer = depositContainer.filter(bank => bank.bankName === "Titan Bank");
              sampsonBankLoanContainer = loanContainer.filter(bank => bank.bankName === "Sampson Bank");
              titanBankLoanContainer = loanContainer.filter(bank => bank.bankName === "Titan Bank");

              // Pushing objects to array field
              $scope.dataSet['Summary Account Information']["DEPOSIT"].banks
                .push(common.buildObjectBankSummary(sampsonBankDepositContainer),common.buildObjectBankSummary(titanBankDepositContainer));

              $scope.dataSet['Summary Account Information']["LOAN"].banks
                .push(common.buildObjectBankSummary(sampsonBankLoanContainer),common.buildObjectBankSummary(titanBankLoanContainer));


              // the total calculated percentage
              var totalPercentage = 0;

              // // get the last group
              // var lastGroup = $scope.dataSet['Summary Account Information']["DEPOSITS"].reverse()[0];
              // $scope.dataSet['Summary Account Information'].reverse();
              //
              // // iterate the groups to fill in the percentage
              // angular.forEach($scope.dataSet['Summary Account Information']['DEPOSITS'], function (group) {
              //
              //     // calculate the percentage
              //     var percentage = Math.floor((group.count / calculatedTotal) * 100);
              //     // add the calculated percentage to the variable
              //     totalPercentage += percentage;
              //
              //     // if this is the last group, apply correction
              //     if (group === lastGroup) {
              //         // because of Math.floor, totalPercentage will not always be 100%, in order to make it right, a correction is applied to the last group
              //         percentage += 100 - totalPercentage;
              //     }
              //
              //     group.precents = percentage + "%";
              //
              //     // apply formatting to amount
              //     group.count = common.addCurrencySymbolToBalance(common.addThousandsSeparator(group.count, ","), "GBP");
              // });
            });

        });

    };

    // declaration of the controller
    var declaration = [
        '$scope',
        'common',
        'accountsService',
        summaryAccountInfomrationController
    ];

    // Create the controller
    angular.module("dinnaco").controller("summaryAccountInfomrationController", declaration);

})();
