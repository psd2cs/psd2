/// <summary>
/// This is the letters Of Credit Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

'use strict';

// self invoking function
(function () {

    // implementation of the letters Of Credit Controller
    var lettersOfCreditController = function ($scope, common, tasksService) {

        // logout clicked
        $scope.logout = function () {
            $scope.mainLogout();
        };

        // check if user is logged in
        $scope.checkForLoggedIn();

        // get id of currently logged in user
        var clientId = $scope.getClientId();

        // set the "inTradesSection property to true, so filtering will be performed when requesting the tasks
        common.setIsInTradesSection(true);

        //Change Main Navigation
        $scope.$emit('main_navigation_change', 'Trade');

        //Defined the data set to store all of data
        $scope.dataSet = {};

        //Get data for panel of 'Tasks'
        //Get the tasks for the client
        tasksService.getAllTasksForClient(clientId).then(function (response) {

            // Filter the tasks so only trade tasks are returned
            var tradeTasks = tasksService.getTradeTasks(response.data);

            // map the tasks to objects used in the UI
            var mappedTasks = [];
            angular.forEach(tradeTasks, function (value) {
                // The account, counterparty, transactions and letter is hard coded (as mentioned in the documentation, this is missing from REST API)
                this.push(new Task(value.taskId, '', value.productCategory, value.type, value.description, value.status, '', value.currency, common.addCurrencySymbolToBalance(common.addThousandsSeparator(value.amount,","), value.currency), '', ''));
            }, mappedTasks);

            $scope.dataSet = {};

            // add the mapped objects to the dataset
            $scope.dataSet['Tasks'] = {
                "heading": ["ID", "Account", "Product Category", "Type", "Task Description", "Status", "Counterparty", "Currency", "Amount", "Transactions", "Letter"],
                "matrix": mappedTasks
            };

            $scope.dataSet['Tasks'].selectSelected = {
                "text": "6"
            };
            $scope.dataSet['Tasks'].selectOptions = [
                { "text": "6" },
                { "text": "12" },
                { "text": "25" }
            ];
            $scope.currentPage = 1;
            $scope.changePagination = function () {
                $scope.currentPage = 1;
            };
        });

        //Get data for panel of 'Latest News'
        var promise = common.makeRequest({
            method: 'GET',
            url: 'data/latest_news.json'
        });
        promise.then(function (data) {
            $scope.dataSet['Latest News'] = data.dataSet['Latest News'];
        });
    };

    // declaration of the controller
    var declaration = [
         '$scope',
         'common',
         'tasksService',
         lettersOfCreditController
    ];

    // Create the controller
    angular.module("dinnaco").controller("lettersOfCreditController", declaration);

})();
