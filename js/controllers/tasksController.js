/// <summary>
/// This is the tasks Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

'use strict';

// self invoking function
(function () {

    // implementation of the tasks Controller
    var tasksController = function ($scope, common, tasksService) {

        // logout clicked
        $scope.logout = function () {
            $scope.mainLogout();
        };

        // check if user is logged in
        $scope.checkForLoggedIn();

        // get id of currently logged in user
        var clientId = $scope.getClientId();

        // Get all the tasks
        tasksService.getAllTasksForClient(clientId).then(function (response) {

            // all the tasks
            var tasksToWorkWith = response.data;

            // if user was on trading section of the app, apply filtering
            if (common.getIsInTradesSection()) {
                tasksToWorkWith = tasksService.getTradeTasks(response.data);
            }

            var mappedTasks = [];

            // map the tasks to object which are used in the UI
            angular.forEach(tasksToWorkWith, function (value) {
                this.push(new Task(value.taskId, '', value.productCategory, value.type, value.description, value.status, '', value.currency, common.addCurrencySymbolToBalance(common.addThousandsSeparator(value.amount,","), value.currency), '', ''));
            }, mappedTasks);

            $scope.dataSet = {};

            // Add data to scope
            $scope.dataSet['Tasks'] = {
                "heading": ["ID", "Account", "Product Category", "Type", "Task Description", "Status", "Counterparty", "Currency", "Amount", "Transactions", "Letter"],
                "matrix": mappedTasks
            };

            $scope.dataSet['Tasks'].selectSelected = {
                "text": "6"
            };
            $scope.dataSet['Tasks'].selectOptions = [
                { "text": "6" },
                { "text": "12" },
                { "text": "25" }
            ];
            $scope.currentPage = 1;
            $scope.changePagination = function () {
                $scope.currentPage = 1;
            };
        });

    };

    // declaration of the controller
    var declaration = [
        '$scope',
        'common',
        'tasksService',
        tasksController
    ];

    // Create the controller
    angular.module("dinnaco").controller("tasksController", declaration);

})();
