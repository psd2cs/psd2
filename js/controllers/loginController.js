/// <summary>
/// This is the login Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

'use strict';

// self invoking function
(function () {

    // implementation of the login Controller
    var loginController = function ($scope, clientsService, $location, $filter, $sessionStorage) {

        // check if user is logged in
        $scope.checkForLoggedIn();

        // Get data
        $scope.login = {
            username: '',
            password: ''
        }
        //Get Username & Password from inputs
        $scope.error = {
            flag: false,
            infor: ''
        };
        $scope.valdate = function () {
            $scope.login.submitted = true;

            // get all the clients & if username == clientId, add the clientId to session and go to the dashboard (=login)
            // if username != clientId, do not login
            clientsService.getAllClients().then(function (response) {
                var clients = response.data;
                // As required in this challenge, there is no validation on password, i comment out the following line, so in next challenges this can be enabled again when needed...
                //if ($filter('filter')(clients, { id: $scope.login.username, password: $scope.login.password }, true).length) {
                if ($filter('filter')(clients, { id: $scope.login.username }, true).length) {
                    $sessionStorage.loginClientId = $scope.login.username;
                    $location.path('/Dashboard');
                } else {
                    if ($scope.login.username && $scope.login.password) {
                        $scope.loginForm.username.$setValidity("size", false);
                        $scope.loginForm.password.$setValidity("size", false);
                    } else {
                        $scope.loginForm.username.$setValidity("size", true);
                        $scope.loginForm.password.$setValidity("size", true);
                    }
                }
            });

        };
        $scope.reset = function () {
            $scope.loginForm.username.$setValidity("size", true);
            $scope.loginForm.password.$setValidity("size", true);
        }

    };

    // declaration of the controller
    var declaration = [
          '$scope',
          'clientsService',
          '$location',
          '$filter',
          '$sessionStorage',
          loginController
    ];

    // Create the controller
    angular.module("dinnaco").controller("loginController", declaration);

})();
