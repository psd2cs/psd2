/// <summary>
/// This is the new Letter Of Credit Created Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

'use strict';

// self invoking function
(function () {

    // implementation of the new Letter Of Credit Created Controller
    var newLetterOfCreditCreatedController = function ($scope,  documentsService) {

        // logout clicked
        $scope.logout = function () {
            $scope.mainLogout();
        };

        // check if user is logged in
        $scope.checkForLoggedIn();

        // get id of currently logged in user
        var clientId = $scope.getClientId();

        // get all the documents
        documentsService.getAllDocumentsForClient(clientId).then(function (response) {

        	function compare(a, b) {
        		if (Number(a.documentId) < Number(b.documentId)) {
        			return -1;
        		}
        		if (Number(a.documentId) > Number(b.documentId)) {
        			return 1;
        		}
        		// a must be equal to b
        		return 0;
        	}

            // get the last document created by sorting on thi id, reversing the array, and the first element should be the latest created document
        	var latest = response.data.sort(compare).reverse()[0];
            // add document to scope
            $scope.credit = latest;
        });
    };

    // declaration of the controller
    var declaration = [
        '$scope',
        'documentsService',
        newLetterOfCreditCreatedController
    ];

    // Create the controller
    angular.module("dinnaco").controller("newLetterOfCreditCreatedController", declaration);

})();
