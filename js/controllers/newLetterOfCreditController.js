/// <summary>
/// This is the new Letter Of Credit Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

'use strict';

// self invoking function
(function () {

    // implementation of the new Letter Of Credit Controller
    var newLetterOfCreditController = function ($scope, $filter, common, clientsService, accountsService, tasksService, documentsService, $sessionStorage) {

        // logout clicked
        $scope.logout = function () {
            $scope.mainLogout();
        };

        // check if user is logged in
        $scope.checkForLoggedIn();

        // get id of currently logged in user
        var clientId = $scope.getClientId();

        //Change Main Navigation
        $scope.$emit('main_navigation_change', 'Trade');

        //Defined the data set to store all of data
        $scope.dataSet = {
            "Step": 1,
            "Amount": {
                "Currency": {}
            },
            "Source Account": {
                "List of Account": [],
                "Best Suggestion": {},
                "Selections": {
                    "data": []
                }
            },
            "Credit Form": {
                "ID": "ID #",
                "Applicant Info": {
                    "expand": true,
                    "Applicant Name": {
                        "value": "Exim Ltd",
                        "required": true,
                        "information": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec massa a orci."
                    },
                    "Address Line 1": {
                        "value": "The Buckley Building",
                        "required": true
                    },
                    "Address Line 2": {
                        "value": "49 Clerkenwell Green",
                        "required": true
                    },
                    "Address Line 3": {
                        "value": "London EC1R 0EB"
                    },
                    "Country": {
                        "value": "United Kingdom",
                        "required": true
                    }
                },
                "Credit Info": {
                    "expand": true,
                    "Currency": {
                        "value": "GBP",
                        "required": true
                    },
                    "Amount": {
                        "value": "100000",
                        "required": true
                    },
                    "Confirm Amount": {
                        "value": "100000",
                        "required": true
                    },
                    "Amount In Word": {
                        "value": ""
                    },
                    "Amount Tolerance": {
                        "selectSelected": {
                            "text": "Please Select"
                        },
                        "selectOptions": [
                            { "text": "10%" },
                            { "text": "20%" },
                            { "text": "100%" }
                        ],
                        "required": true
                    },
                    "Advising Bank Name": {
                        "value": "",
                        "information": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec massa a orci."
                    },
                    "Swift Address": {
                        "value": ""
                    },
                    "Address Line 1": {
                        "value": ""
                    },
                    "Address Line 2": {
                        "value": ""
                    },
                    "Address Line 3": {
                        "value": ""
                    },
                    "Country": {
                        "value": "United Kingdom",
                        "required": true
                    }
                },
                "Submission Details": {
                    "expand": true,
                    "Date of Submission": {
                        "value": "",
                        "required": true
                    },
                    "Method of Submission": {
                        "selectSelected": {
                            "text": "Please Select"
                        },
                        "selectOptions": [
                            { "text": "Web" },
                            { "text": "Other" }
                        ],
                        "required": true,
                        "information": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec massa a orci."
                    },
                    "Applicant Reference": {
                        "value": "ABC banks",
                        "information": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec massa a orci.",
                        "required": true
                    },
                    "Telephone": {
                        "value": "+4420 893921",
                        "required": true
                    },
                    "Fax": {
                        "value": ""
                    },
                    "Contact Name": {
                        "value": "John Franklin",
                        "required": true,
                        "information": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec massa a orci."
                    },
                    "Email Address": {
                        "value": ""
                    },
                    "Type of Irrevocable Letter of Credit": {
                        "Unconfirmed": {
                            "name": "",
                            "information": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec massa a orci."
                        },
                        "Transferable": {
                            "name": "",
                            "information": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec massa a orci."
                        }
                    }
                },
                "Beneficiary Info": {
                    "expand": true,
                    "Beneficiary Name": {
                        "value": "ABC manufactuting",
                        "required": true,
                        "information": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec massa a orci."
                    },
                    "Amount": {
                        "value": "100000",
                        "required": true
                    },
                    "Address Line 1": {
                        "value": "67 Bishop Avenue",
                        "required": true
                    },
                    "Address Line 2": {
                        "value": "SW3 4RF",
                        "required": true
                    },
                    "Address Line 3": {
                        "value": "London"
                    },
                    "Country": {
                        "value": "United Kingdom",
                        "required": true
                    },
                    "Contact Name": {
                        "value": ""
                    },
                    "Account Number": {
                        "value": ""
                    },
                    "Telephone": {
                        "value": "+4420 9832231",
                        "required": true
                    },
                    "Fax": {
                        "value": ""
                    }
                },
                "Shipment Details": {
                    "expand": true,
                    "Latest Shipment Date": {
                        "value": "",
                        "required": true
                    },
                    "Expiry Date": {
                        "value": ""
                    },
                    "Part-shipment": {
                        "selectSelected": {
                            "text": "Please Select"
                        },
                        "selectOptions": [
                            { "text": "Allowed" },
                            { "text": "Non-allowed" }
                        ],
                        "required": true
                    },
                    "Payment Terms": {
                        "selectSelected": {
                            "text": "Please Select"
                        },
                        "selectOptions": [
                            { "text": "Payment Terms 1" },
                            { "text": "Payment Terms 2" }
                        ],
                        "required": true
                    },
                    "Invoice Value": {
                        "value": "20",
                        "required": true
                    },
                    "Days": {
                        "value": "34",
                        "required": true
                    },
                    "Place of Expiry": {
                        "value": "Accounts",
                        "required": true
                    },
                    "Transhipment": {
                        "selectSelected": {
                            "text": "Please Select"
                        },
                        "selectOptions": [
                            { "text": "Payment Terms 1" },
                            { "text": "Payment Terms 2" }
                        ],
                        "required": true
                    }
                }
            }
        };

        //Get Currency from JSON
        var promise = common.makeRequest({
            method: 'GET',
            url: 'data/currency.json'
        });
        promise.then(function (data) {
            $scope.dataSet['Amount']['Currency'] = angular.extend($scope.dataSet['Amount']['Currency'], data['Currency']);
        });
        //Get Best Suggestion from JSON
        var promise = common.makeRequest({
            method: 'GET',
            url: 'data/best_suggestion.json'
        });
        promise.then(function (data) {

            $scope.dataSet['Source Account']['Best Suggestion'] = angular.extend($scope.dataSet['Source Account']['Best Suggestion'], data['Best Suggestion']);
        });

        //Get List of Accounts
        accountsService.getAccountsForClient(clientId).then(function (response) {
          var depositContainer, loanContainer, mappedAccounts;

          // getting only first 4 accounts of each type,
          // sorted by ascending balance,
          // makes sure always will be enough money using the percentage in best suggestions
          depositContainer = response.data
          .filter( account => account.accountType === 'DEPOSIT')
          .map(depositAccount => {
                depositAccount = new Account(depositAccount.id, depositAccount.productName, depositAccount.balance, depositAccount.accountType, depositAccount.notes);
                return depositAccount;
            })
          .sort((a,b) => {return b.balance-a.balance})
          .slice(0, 4);

          loanContainer = response.data
          .filter( account => account.accountType === 'LOAN')
          .map(loanAccount => {
                loanAccount = new Account(loanAccount.id, loanAccount.productName, loanAccount.balance, loanAccount.accountType, loanAccount.notes);
                return loanAccount;
            })
          .sort((a,b)=>{return b.balance-a.balance})
          .slice(0, 4);


          // Map them to objects used in the UI
          mappedAccounts = depositContainer.concat(loanContainer);


          // Add mapped objects to the dataset and sorted by ascending balance,
          // makes sure always will be enough money using the percentage in best suggestions
          $scope.dataSet['Source Account']['List of Account'] = angular.extend($scope.dataSet['Source Account']['List of Account'], mappedAccounts);
        });

        //Filter Object by Key
        $scope.getObject = function (array, string) {
            return array.filter(function (array) {
                return array.name == string;
            });
        }

        //Select Account from List of Accounts
        $scope.selectAccount = function (item) {
            item.selected = !item.selected;
            item.value = 0;
            if (item.selected) {
                //Add Element to Selections
                $scope.dataSet['Source Account']['Selections'].data.push(item);
            } else {
                //Remove Element from Selections
                $scope.dataSet['Source Account']['Selections'].data = $.grep($scope.dataSet['Source Account']['Selections'].data, function (e) {
                    return e.name != item.name;
                });
            }
        }
        //Count
        $scope.count = function () {
            var amount = 0,
                valid;
            angular.forEach($scope.dataSet['Source Account']['Selections'].data, function (e, i) {
                amount += parseInt(e.value);
            });
            amount === parseInt($scope.dataSet['Amount'].Value) ? valid = false : valid = true;
            return valid;
        };
        //Delete Account from List of Accounts
        $scope.deleteAccount = function (item) {
            $scope.$emit('modal_comfirm_removal', item);
        }
        $scope.$on('comfirm_removal', function (event, data) {
            var index = $scope.dataSet['Source Account']['List of Account'].indexOf(data);
            $scope.dataSet['Source Account']['List of Account'].splice(index, 1);
            //Remove Element from Selections
            $scope.dataSet['Source Account']['Selections'].data = $.grep($scope.dataSet['Source Account']['Selections'].data, function (e) {
                return e.name != data.name;
            });
        });
        //Delete Selected from Selections
        $scope.deleteSelected = function (item) {
          var deselectedItem = _.find( $scope.dataSet['Source Account']['Selections'].data, {id: item.id});
          item.value = 0;
          item.selected = false;

          angular.forEach($scope.dataSet['Source Account']['Selections'].data, function(element, index, array) {
            if (deselectedItem.id === element.id) {
                array.splice(index, 1);
            }
          });
        }
        //Apply
        $scope.apply = function () {
            var _sum = 0;
            var selectedAccounts;

            angular.forEach($scope.dataSet['Source Account']['List of Account'], function (obj, index) {
                if (index < $scope.dataSet['Source Account']['Best Suggestion'].data.length ) {
                    obj['value'] = parseInt($scope.dataSet['Source Account']['Best Suggestion'].data[index].percentage * $scope.dataSet['Amount'].Value);
                    _sum += parseInt($scope.dataSet['Source Account']['Best Suggestion'].data[index].percentage * $scope.dataSet['Amount'].Value);
                    obj['selected'] = true;
                } else {
                    obj['value'] = $scope.dataSet['Amount'].Value - _sum;
                    obj['selected'] = false;
                }
            });
            // Only adding those accounts to source account selections, where (selected === true)
            selectedAccounts = $scope.dataSet['Source Account']['List of Account'].filter(account => account.selected === true);
            $scope.dataSet['Source Account']['Selections'].data = selectedAccounts;
        }
        //Back Function
        $scope.back = function () {
            $scope.dataSet.Step--;
            $("html, body").animate({ scrollTop: 0 }, '500', 'swing');
        }
        //MoveTo Function
        $scope.moveTo = function (i) {
            if (i < $scope.dataSet.Step) {
                $scope.dataSet.Step = i;
                $("html, body").animate({ scrollTop: 0 }, '500', 'swing');
            }
        }
        //Next Function
        $scope.next = function () {
            $scope.dataSet.Step++;
            $("html, body").animate({ scrollTop: 0 }, '500', 'swing');
        }

        // Faking put request to accounts
        $sessionStorage.selectedAccounts = [];
        //Confirm Function
        $scope.confirm = function () {

          // Faking put request to accounts
          angular.forEach($scope.dataSet['Source Account']['Selections'].data, function (e, i) {
              // Getting new balances for the selected accounts
              if (e.value > 0) {
              var accountFromSourceList =  _.find($scope.dataSet['Source Account']['List of Account'], {id: e.id});
                accountFromSourceList.balance = Number(accountFromSourceList.balance - e.value);
                $sessionStorage.selectedAccounts.push(e);
              }
          });

            // Create a new task
            var newTask = {
                productCategory: "Account",
                type: "Import Letter of Credit",
                description: "",
                status: "draft",
                currency: $scope.dataSet['Amount']['Currency'].selectSelected.text,
                amount: $scope.dataSet['Amount'].Value
            };

            // post the new task
            tasksService.createNewTaskForClient(clientId, newTask).then(function (data) {

                // when task is created, get the last created task id
                tasksService.getAllTasksForClient(clientId).then(function (response) {

                	function compare(a, b) {
                		if (Number(a.taskId) < Number(b.taskId)) {
                			return -1;
                		}
                		if (Number(a.taskId) > Number(b.taskId)) {
                			return 1;
                		}
                		// a must be equal to b
                		return 0;
                	}

                    // order the data on id, and reverse the array. The first one should be the last created task
                	var latest = response.data.sort(compare).reverse()[0];
                    $scope.dataSet["Credit Form"]["ID"] = "ID #" + latest.taskId;

                    // goto next step
                    $scope.dataSet.Step++;
                    $("html, body").animate({ scrollTop: 0 }, '500', 'swing');
                });

            });
        }

        $scope.amount_next = function () {
            // Container for accounts with biggest balances
            var bestSuggestions;
            $scope.dataSet.Step++;
            if ($scope.dataSet['Amount'].changed) {
                $scope.dataSet['Source Account']['Selections'].data = [];

                // Pre-populate values on form details new credit letter
                $scope.dataSet['Credit Form']['Credit Info']['Amount Tolerance'].selectSelected.text = $scope.dataSet['Credit Form']['Credit Info']['Amount Tolerance'].selectOptions[1].text;
                $scope.dataSet['Credit Form']['Submission Details']['Method of Submission'].selectSelected.text = $scope.dataSet['Credit Form']['Submission Details']['Method of Submission'].selectOptions[1].text;
                $scope.dataSet['Credit Form']['Submission Details']['Type of Irrevocable Letter of Credit']["Unconfirmed"].name = "Confirmed";
                $scope.dataSet['Credit Form']['Submission Details']['Type of Irrevocable Letter of Credit']["Transferable"].name = "Transferable";
                $scope.dataSet['Credit Form']['Shipment Details']['Part-shipment'].selectSelected.text = $scope.dataSet['Credit Form']['Shipment Details']['Part-shipment'].selectOptions[1].text;

                $scope.dataSet['Credit Form']['Shipment Details']['Payment Terms'].selectSelected.text = $scope.dataSet['Credit Form']['Shipment Details']['Payment Terms'].selectOptions[1].text;

                $scope.dataSet['Credit Form']['Shipment Details']['Transhipment'].selectSelected.text = $scope.dataSet['Credit Form']['Shipment Details']['Transhipment'].selectOptions[1].text;

                $scope.dataSet['Source Account']['List of Account'].forEach(function (item, index, array) {
                    item.selected = false;
                    item.value = undefined;

                    // Deleting all negatives balances
                    if (Math.sign(Number(item.balance)) === -1) {
                      array.splice(index, 1)
                    }
                });

                $scope.dataSet['Amount'].changed = false;
                bestSuggestions = $scope.dataSet['Source Account']['List of Account'].slice(0, 4);
                bestSuggestions[0].percentage = 0.39;
                bestSuggestions[1].percentage = 0.25;
                bestSuggestions[2].percentage = 0.2;
                bestSuggestions[3].percentage = 0.16;

                $scope.dataSet['Source Account']['Best Suggestion'].data = angular.extend($scope.dataSet['Source Account']['Best Suggestion'].data, bestSuggestions);
            }
            $("html, body").animate({ scrollTop: 0 }, '500', 'swing');
        }
        //Expand
        $scope.expand = function () {
            $scope.dataSet['Credit Form']['Applicant Info'].expand = true;
            $scope.dataSet['Credit Form']['Credit Info'].expand = true;
            $scope.dataSet['Credit Form']['Submission Details'].expand = true;
            $scope.dataSet['Credit Form']['Beneficiary Info'].expand = true;
            $scope.dataSet['Credit Form']['Shipment Details'].expand = true;
        }
        //Collpase
        $scope.collpase = function () {
            $scope.dataSet['Credit Form']['Applicant Info'].expand = false;
            $scope.dataSet['Credit Form']['Credit Info'].expand = false;
            $scope.dataSet['Credit Form']['Submission Details'].expand = false;
            $scope.dataSet['Credit Form']['Beneficiary Info'].expand = false;
            $scope.dataSet['Credit Form']['Shipment Details'].expand = false;
        }
        //Submit Review
        $scope.submitReview = function () {
            $scope.dataSet['Credit Form'].submitted = true;
            if ($scope.applicant_info_form.$valid && $scope.credit_info_form.$valid && $scope.submission_details_form.$valid && $scope.beneficiary_info_form.$valid && $scope.shipment_details_form.$valid && $scope.dataSet['Credit Form']['Credit Info']['Amount Tolerance'].selectSelected.text !== 'Please Select' && $scope.dataSet['Credit Form']['Submission Details']['Method of Submission'].selectSelected.text !== 'Please Select' && $scope.dataSet['Credit Form']['Shipment Details']['Part-shipment'].selectSelected.text !== 'Please Select' && $scope.dataSet['Credit Form']['Shipment Details']['Payment Terms'].selectSelected.text !== 'Please Select' && $scope.dataSet['Credit Form']['Shipment Details']['Transhipment'].selectSelected.text !== 'Please Select' && $scope.dataSet['Credit Form']['Submission Details']['Type of Irrevocable Letter of Credit']['Unconfirmed'].name && $scope.dataSet['Credit Form']['Submission Details']['Type of Irrevocable Letter of Credit']['Transferable'].name) {
                $scope.dataSet.Step = 4;
                $("html, body").animate({ scrollTop: 0 }, '500', 'swing');
            }
        }
        $scope.dateOptions = {
            formatYear: 'yy',
            formatDay: 'd',
            startingDay: 1
        };
        //Open Datepicker
        $scope.openDatepicker = function (item) {
            item.opened = true;
        }

        //Credit Created
        $scope.created = function (item) {

            // Get the correct format of the submission date
            var submissionDate = $scope.dataSet['Credit Form']['Submission Details']["Date of Submission"].value;
            var submissionDateFormatted = $filter('date')(new Date(submissionDate), "yyyy-MM-dd") + "T00:00:00+0000";

            // get the correct format of the latest shipment date
            var latestShipmentDate = $scope.dataSet['Credit Form']['Shipment Details']["Latest Shipment Date"].value;
            var latestShipmentDateFormatted = $filter('date')(new Date(latestShipmentDate), "yyyy-MM-dd") + "T00:00:00+0000";

            // get the correct format of the expiry date
            var expiryDate = $scope.dataSet['Credit Form']['Shipment Details']["Expiry Date"].value;
            var expiryDateFormatted = $filter('date')(new Date(expiryDate), "yyyy-MM-dd") + "T00:00:00+0000";

            // create a new document
            var newDocument = {
                applicantReference: $scope.dataSet['Credit Form']['Submission Details']["Applicant Reference"].value,
                applicantName: $scope.dataSet['Credit Form']['Applicant Info']["Applicant Name"].value,
                applicantAddressLine1: $scope.dataSet['Credit Form']['Applicant Info']["Address Line 1"].value,
                applicantAddressLine2: $scope.dataSet['Credit Form']['Applicant Info']["Address Line 2"].value,
                applicantAddressLine3: $scope.dataSet['Credit Form']['Applicant Info']["Address Line 3"].value,
                applicantAddressCountry: $scope.dataSet['Credit Form']['Applicant Info']["Country"].value,

                applicantTelephone: $scope.dataSet['Credit Form']['Submission Details']["Telephone"].value,
                applicantFax: $scope.dataSet['Credit Form']['Submission Details']["Fax"].value,
                applicantEmailAddress: $scope.dataSet['Credit Form']['Submission Details']["Email Address"].value,
                applicantContactName: $scope.dataSet['Credit Form']['Submission Details']["Contact Name"].value,

                creditCurrency: $scope.dataSet['Credit Form']['Credit Info']["Currency"].value,
                creditAmount: $scope.dataSet['Credit Form']['Credit Info']["Amount"].value,
                creditAmountInWords: $scope.dataSet['Credit Form']['Credit Info']["Amount In Word"].value,
                creditAmountTolerance: $scope.dataSet['Credit Form']['Credit Info']["Amount Tolerance"]["selectSelected"].text,

                advisingBankName: $scope.dataSet['Credit Form']['Credit Info']["Advising Bank Name"].value,
                advisingBankSwiftAddress: $scope.dataSet['Credit Form']['Credit Info']["Swift Address"].value,
                advisingBankAddressLine1: $scope.dataSet['Credit Form']['Credit Info']["Address Line 1"].value,
                advisingBankAddressLine2: $scope.dataSet['Credit Form']['Credit Info']["Address Line 2"].value,
                advisingBankAddressLine3: $scope.dataSet['Credit Form']['Credit Info']["Address Line 3"].value,
                advisingBankCountry: $scope.dataSet['Credit Form']['Credit Info']["Country"].value,

                irrevocableLocTypeConfirmed: $scope.dataSet['Credit Form']['Submission Details']['Type of Irrevocable Letter of Credit']['Unconfirmed'].name === 'Unconfirmed' ? false : true,
                irrevocableLocTypeTransferable: $scope.dataSet['Credit Form']['Submission Details']['Type of Irrevocable Letter of Credit']['Transferable'].name === 'Transferable' ? true : false,

                submissionDate: submissionDateFormatted,
                submissionMethod: $scope.dataSet['Credit Form']['Submission Details']["Method of Submission"]["selectSelected"].text,

                beneficiaryName: $scope.dataSet['Credit Form']['Beneficiary Info']["Beneficiary Name"].value,
                beneficiaryAccountNumber: $scope.dataSet['Credit Form']['Beneficiary Info']["Account Number"].value,
                beneficiaryContactName: $scope.dataSet['Credit Form']['Beneficiary Info']["Contact Name"].value,
                beneficiaryAddressLine1: $scope.dataSet['Credit Form']['Beneficiary Info']["Address Line 1"].value,
                beneficiaryAddressLine2: $scope.dataSet['Credit Form']['Beneficiary Info']["Address Line 2"].value,
                beneficiaryAddressLine3: $scope.dataSet['Credit Form']['Beneficiary Info']["Address Line 3"].value,
                beneficiaryAddressCountry: $scope.dataSet['Credit Form']['Beneficiary Info']["Country"].value,
                beneficiaryTelephone: $scope.dataSet['Credit Form']['Beneficiary Info']["Telephone"].value,
                beneficiaryFax: $scope.dataSet['Credit Form']['Beneficiary Info']["Fax"].value,

                latestShipmentDate: latestShipmentDateFormatted,
                documentsPresentedWithinDays: $scope.dataSet['Credit Form']['Shipment Details']["Days"].value,

                expiryDate: expiryDateFormatted,
                placeOfExpiry: $scope.dataSet['Credit Form']['Shipment Details']["Place of Expiry"].value,
                partShipment: $scope.dataSet['Credit Form']['Shipment Details']["Part-shipment"]["selectSelected"].text,
                tranShipment: $scope.dataSet['Credit Form']['Shipment Details']["Transhipment"]["selectSelected"].text,
                paymentTerms: $scope.dataSet['Credit Form']['Shipment Details']["Payment Terms"]["selectSelected"].text,
                percentageOfInvoiceValue: $scope.dataSet['Credit Form']['Shipment Details']["Invoice Value"].value
            };

            // post the new document
            documentsService.createNewDocumentForClient(clientId, newDocument).then(function (response) {
                // navigate to the confirmation dialog
                $scope.$emit('modal_credit_created', item);
            });
        }
    };

    // declaration of the controller
    var declaration = [
        '$scope',
        '$filter',
        'common',
        'clientsService',
        'accountsService',
        'tasksService',
        'documentsService',
        '$sessionStorage', // faking put request to accounts
        newLetterOfCreditController
    ];

    // Create the controller
    angular.module("dinnaco").controller("newLetterOfCreditController", declaration);

})();
