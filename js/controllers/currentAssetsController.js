/// <summary>
/// This is the current Assets Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

'use strict';

// self invoking function
(function () {

    // implementation of the current Assets Controller
    var currentAssetsController = function ($scope, common, accountsService) {

        // logout clicked
        $scope.logout = function () {
            $scope.mainLogout();
        };

        // check if user is logged in
        $scope.checkForLoggedIn();

        // get id of currently logged in user
        var clientId = $scope.getClientId();

        //Defined the data set to store all of data
        $scope.dataSet = {};

        //Get data for Current Assets
        var promise = common.makeRequest({
            method: 'GET',
            url: 'data/current_assets.json'
        });
        promise.then(function (data) {
            $scope.dataSet = data.dataSet;

            // Get all the accounts of the client
            accountsService.getAccountsForClient(clientId).then(function (response) {
                // The total of this group
                var totalBankHeldAssets = 0;
                var totalBankHeldLiabilities = 0;
                var totalNetAssets = 0;
                // Group for all the deposit accounts
                var depositGroup;
                // Group for all the loan accounts
                var loanGroup;
                // The deposit accounts
                var depositAccounts = [];
                // The loan accounts
                var loanAccounts = [];

                // split up the accounts in the corresponding array according to the account type
                // for this challenge, only LOAN and DEPOSIT type was required
                angular.forEach(response.data, function (value) {
                    if (value.accountType === "DEPOSIT") {
                        depositAccounts.push(new Account(value.id, value.productName, value.balance, value.accountType, value.notes));
                        totalBankHeldAssets += Number(value.balance);
                    } else if (value.accountType === "LOAN") {
                        loanAccounts.push(new Account(value.id, value.productName, value.balance, value.accountType, value.notes));
                        totalBankHeldLiabilities += Number(value.balance);
                    }
                });

                console.log(depositAccounts,'depositAccounts');
                console.log(loanAccounts,'loanAccounts');
                console.log(response.data,'response.data');

                // fill in the total amount of the entire group
                $scope.dataSet[0].amount = common.transformBalanceToHighDigitNumberWithCurrencySymbol(totalBankHeldAssets, "GBP");
                $scope.dataSet[1].amount = common.transformBalanceToHighDigitNumberWithCurrencySymbol(totalBankHeldLiabilities, "GBP");

                // Grouping deposit accounts by name property
                depositGroup = _.groupBy(depositAccounts, 'name');
                // Grouping loan accounts by name property
                loanGroup = _.groupBy(loanAccounts, 'name');

                // Adding each type of account to it's own group
                angular.forEach($scope.dataSet[0].group, function(accountType) {
                  if ( accountType.name === depositGroup[accountType.name][0].name) {
                    accountType.accounts.push(common.buildObjectAccountCurrentAssets(depositGroup[accountType.name]));
                  }
                });

                // Adding each type of account to it's own group
                angular.forEach($scope.dataSet[1].group, function(accountType) {
                  if ( accountType.name === loanGroup[accountType.name][0].name) {
                    accountType.accounts.push(common.buildObjectAccountCurrentAssets(loanGroup[accountType.name]));
                  }
                });

                // Getting total sum for different types of deposit accounts
                _.each( depositGroup, (containerTypeAccount) => {
                  _.reduce( containerTypeAccount, (result, value, key) => {
                    console.log(value.name);
                    var depositCount = _.find($scope.dataSet[0].group, {name: value.name});
                    depositCount.count +=  Number(value.balance);
                  }, 0);
                });

                // Getting total sum for different types of loan accounts
                _.each( loanGroup, (containerTypeAccount) => {
                  _.reduce( containerTypeAccount, (result, value, key) => {
                    var loanCount = _.find($scope.dataSet[1].group, {name: value.name});
                    loanCount.count +=  Number(value.balance);
                  }, 0);
                });

                // Formatting the Account's total values to a more readable string
                $scope.dataSet[0].group.forEach(account => {
                  account.count = common.transformBalanceToHighDigitNumberWithCurrencySymbol(account.count, "GBP");
                });
                $scope.dataSet[1].group.forEach(account => {
                  account.count = common.transformBalanceToHighDigitNumberWithCurrencySymbol(account.count, "GBP");
                });

                // Setting net assets values
                totalNetAssets = totalBankHeldAssets - totalBankHeldLiabilities;
                $scope.dataSet[2].amount = common.transformBalanceToHighDigitNumberWithCurrencySymbol(totalNetAssets, "GBP");

                console.log('common.transformBalanceToHighDigitNumberWithCurrencySymbol(totalNetAssets, "GBP")',common.transformBalanceToHighDigitNumberWithCurrencySymbol(totalNetAssets, "GBP"));

                // fill in the account data in the correct group of the dataset
                // angular.forEach($scope.dataSet[0].group, function (group) {
                //     // the total ammount of the current group
                //     var groupTotal = 0;
                //     if (group.name === "Loan Positions") {
                //         // clear the group
                //         group.group = [];
                //         // add this account to the group + increment the group total with this amount
                //         angular.forEach(loanAccounts, function (loanAccount) {
                //             groupTotal += Number(loanAccount.balance);
                //             group.group.push({ "bankImage": loanAccount.bankImage, "name": "Account " + loanAccount.id, "amount": common.addCurrencySymbolToBalance(common.addThousandsSeparator(loanAccount.balance,","),"GBP") });
                //         });
                //         // fill in the total amount of this group
                //         group.amount = common.transformBalanceToHighDigitNumberWithCurrencySymbol(groupTotal,"GBP");
                //     } else if (group.name === "Deposit Accounts") {
                //         // clear the group
                //         group.group = [];
                //         // add this account to the group + increment the group total with this amount
                //         angular.forEach(depositAccounts, function(depositAccount) {
                //             groupTotal += Number(depositAccount.balance);
                //             group.group.push({ "bankImage": depositAccount.bankImage, "name": "Account " + depositAccount.id, "amount": common.addCurrencySymbolToBalance(common.addThousandsSeparator(depositAccount.balance, ","), "GBP") });
                //         });
                //         // fill in the total amount of this group
                //         group.amount = common.transformBalanceToHighDigitNumberWithCurrencySymbol(groupTotal, "GBP");
                //     }
                // });
            });
        });


        //Trigger Accroidion
        $scope.triggerAccrodion = function (item) {
            if (item.accorion) {
                item.accorion = false;
            } else {
                item.accorion = true;
            }
        }
        $scope.triggerSecondaryAccrodion = function (item) {
            if (item.accorion) {
                item.accorion = false;
            } else {
                item.accorion = true;
            }
        }
    };

    // declaration of the controller
    var declaration = [
        '$scope',
        'common',
        'accountsService',
        currentAssetsController
    ];

    // Create the controller
    angular.module("dinnaco").controller("currentAssetsController", declaration);

})();
