/// <summary>
/// This is the latest News Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

'use strict';

// self invoking function
(function () {

    // implementation of the latest News Controller
    var latestNewsController = function ($scope, common) {

        // logout clicked
        $scope.logout = function () {
            $scope.mainLogout();
        };

        // check if user is logged in
        $scope.checkForLoggedIn();

        //Get data for panel of 'Latest News'
        var promise = common.makeRequest({
            method: 'GET',
            url: 'data/latest_news.json'
        });
        promise.then(function (data) {
            $scope.dataSet = {};
            $scope.dataSet['Latest News'] = data.dataSet['Latest News'];
        });
    };

    // declaration of the controller
    var declaration = [
        '$scope',
        'common',
        latestNewsController
    ];

    // Create the controller
    angular.module("dinnaco").controller("latestNewsController", declaration);

})();
