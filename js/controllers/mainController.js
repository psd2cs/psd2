/// <summary>
/// This is the main Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

'use strict';

// self invoking function
(function () {

    // implementation of the main Controller
    var mainController = function ($scope,$log, $rootScope, $location, $sessionStorage) {

        //Modal for Comfirm Removal
        $scope.$on('modal_comfirm_removal', function (event, data) {
            $scope.modal = {
                show: true,
                dialog: 'Comfirm Removal',
                data: data
            }
        });
        //Close Modal
        $scope.closeModal = function () {
            $scope.modal = {
                show: false
            }
        }
        $scope.$on('modal_close', function (event, data) {
            $scope.modal = {
                show: false,
                dialog: ''
            }
            $scope.$digest();
        });
        //Remove Account
        $scope.removeAccount = function (item) {
            $scope.modal = {
                show: false
            }
            $scope.$broadcast('comfirm_removal', item);
        }
        //Modal for New Letter of Credit Created
        $scope.$on('modal_credit_created', function (event, data) {
            $location.path('/Trade');
            $scope.modal = {
                show: true,
                dialog: 'Credit Created',
                data: data
            }
        });
        //Modal for Current Assets
        $scope.$on('modal_current_assets', function (event, data) {
            $scope.modal = {
                show: true,
                dialog: 'Current Assets'
            }
        });
        //Modal for Summary Account Information
        $scope.$on('modal_summary_account_information', function (event, data) {
            $scope.modal = {
                show: true,
                dialog: 'Summary Account Information'
            }
        });
        //Modal for Assets Liabilities
        $scope.$on('modal_assets_liabilities', function (event, data) {
            $scope.modal = {
                show: true,
                dialog: 'Assets & Liabilities'
            }
        });
        //Modal for Recommended Actions
        $scope.$on('modal_recommoende_actions', function (event, data) {
            $scope.modal = {
                show: true,
                dialog: 'Recommended Actions'
            }
        });
        //Modal for Tasks
        $scope.$on('modal_tasks', function (event, data) {
            $scope.modal = {
                show: true,
                dialog: 'Tasks'
            }
        });
        //Modal for Calendar
        $scope.$on('modal_canlendar', function (event, data) {
            $scope.modal = {
                show: true,
                dialog: 'Canlendar'
            }
        });
        //Modal for Rates
        $scope.$on('modal_rates', function (event, data) {
            $scope.modal = {
                show: true,
                dialog: 'Rates'
            }
        });
        //Modal for Latest News
        $scope.$on('modal_latest_news', function (event, data) {
            $scope.modal = {
                show: true,
                dialog: 'Latest News'
            }
        });
        //Modal for Outstanding Payables
        $scope.$on('modal_outstanding_payables', function (event, data) {
            $scope.modal = {
                show: true,
                dialog: 'Outstanding Payables'
            }
        });
        //Modal for Trade - Investment by Product
        $scope.$on('modal_trade_investment', function (event, data) {
            $scope.modal = {
                show: true,
                dialog: 'Trade Investment'
            }
        });
        //Modal for Import Letter of Credit
        $scope.$on('modal_import_letter_credit', function (event, data) {
            $scope.modal = {
                show: true,
                dialog: 'Import Letter of Credit'
            }
        });
        //Modal for Trade Tasks
        $scope.$on('modal_trade_tasks', function (event, data) {
            $scope.modal = {
                show: true,
                dialog: 'Trade Tasks'
            }
        });
        //Main Navigation
        $scope.$on('main_navigation_change', function (event, data) {
            $scope.navigation = {
                label: data
            }
        });
        //Log out => Delete the session (and thus also the session variable clientId)
        $scope.mainLogout = function () {
            $log.debug('logging out');
            $sessionStorage.$reset();
            $rootScope.goto('');
        };
        //Check if the user is logged in, if not, navigate to the login page.
        $scope.checkForLoggedIn = function () {
            $log.debug('check if logged in');

            if (!('loginClientId' in $sessionStorage)) {
                $rootScope.goto('');
            }
        };
        //Method te return the clientId of the logged in user
        $scope.getClientId = function() {
            return $sessionStorage.loginClientId;
        }

    };

    // declaration of the controller
    var declaration = [
        '$scope',
        '$log',
        '$rootScope',
        '$location',
        '$sessionStorage',
        mainController
    ];

    // Create the controller
    angular.module("dinnaco").controller("mainController", declaration);

})();
