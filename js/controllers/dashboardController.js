/// <summary>
/// This is the dashboard Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

'use strict';

// self invoking function
(function () {

    // implementation of the dashboard Controller
    var dashboardController = function ($scope, common, tasksService, accountsService, clientsService, $sessionStorage) {

        // logout clicked
        $scope.logout = function () {
            $scope.mainLogout();
        };

        // check if user is logged in
        $scope.checkForLoggedIn();

        // get id of currently logged in user
        var clientId = $scope.getClientId();

        // set the "inTradesSection property to false, so no filtering will be performed when requesting the tasks
        common.setIsInTradesSection(false);

        //Change Main Navigation
        $scope.$emit('main_navigation_change', 'Home');

        //Defined the data set to store all of data
        $scope.dataSet = {};

        // get object client of currently logged in user
        clientsService.getClient(clientId).then(function(response){
          $scope.client = response.data;
        });

        // Get all the accounts of the user
        var promise = common.makeRequest({
            method: 'GET',
            url: 'data/summary_account_information.json'
        });
        promise.then(function (data) {

            $scope.dataSet = {};
            $scope.dataSet['Summary Account Information'] = data.dataSet['Summary Account Information'];

            //Get data for panel of 'Summary Account Information'
            // Get all accounts of the client
            accountsService.getAccountsForClient(clientId).then(function (response) {

              // Faking put request to accounts
              if($sessionStorage.selectedAccounts !== undefined && $sessionStorage.selectedAccounts.length > 0) {
                console.log('$sessionStorage.selectedAccounts.length > 0');
                angular.forEach($sessionStorage.selectedAccounts, function(account){
                  var responseAccount = _.find(response.data, {id: account.id});
                  console.log(account, 'account');
                  console.log(responseAccount, 'responseAccount');
                  responseAccount.balance = account.balance;
                });
              }

                // The deposit accounts total count
                var depositBalance = 0;
                // The loan accounts total count
                var loanBalance = 0;
                // Group for all the deposit accounts
                var depositGroup;
                // Group for all the loan accounts
                var loanGroup;
                // bank containers
                var sampsonBankDepositContainer;
                var sampsonBankLoanContainer;
                var titanBankDepositContainer;
                var titanBankLoanContainer;

                // Retrieving the actual data structure so we can set some of the properties
                var depositDataSetAccounts = $scope.dataSet['Summary Account Information']["DEPOSIT"]["accounts"];
                var loanDataSetAccounts = $scope.dataSet['Summary Account Information']["LOAN"]["accounts"];

                // Temp arrays to split accounts into loan and deposit types and
                // modify each entry of array so it'll hold an Account object
                var activeAccounts = response.data.filter( account => account.state.includes('ACTIVE'));

                var depositContainer = activeAccounts.filter( account => account.accountType === 'DEPOSIT')
                  .map(depositAccount => {
                      depositAccount = new Account(depositAccount.id, depositAccount.productName, depositAccount.balance, depositAccount.accountType, depositAccount.notes);
                      return depositAccount;
                  });

                var loanContainer = activeAccounts.filter( account => account.accountType === 'LOAN')
                  .map(loanAccount => {
                      loanAccount = new Account(loanAccount.id, loanAccount.productName, loanAccount.balance, loanAccount.accountType, loanAccount.notes);
                      return loanAccount;
                  });

                console.log(depositContainer, 'depositContainer',$scope);
                // console.log(loanContainer, 'loanContainer');

                // Calculate total count for all deposit accounts
                depositContainer.forEach(depositAccount => {
                  depositBalance += Number(depositAccount.balance);
                  return depositBalance;
                });

                // Calculate total count for all loan accounts
                loanContainer.forEach(loanAccount => {
                  loanBalance += Number(loanAccount.balance);
                  return loanBalance;
                });

                // Setting new totalCount values for deposit and loan accounts
                $scope.dataSet['Summary Account Information']["DEPOSIT"].totalCount = common.transformBalanceToHighDigitNumberWithCurrencySymbol(depositBalance, "GBP");
                $scope.dataSet['Summary Account Information']["LOAN"].totalCount = common.transformBalanceToHighDigitNumberWithCurrencySymbol(loanBalance, "GBP");

                // console.log(depositBalance, 'depositBalance');
                // console.log(loanBalance, 'loanBalance');

                // Grouping deposit accounts by name property
                depositGroup = _.groupBy(depositContainer, 'name');
                // Grouping loan accounts by name property
                loanGroup = _.groupBy(loanContainer, 'name');

                // Getting total sum for different types of deposit accounts
                _.each( depositGroup, (containerTypeAccount) => {
                  _.reduce( containerTypeAccount, (result, value, key) => {
                    var depositCount = _.find(depositDataSetAccounts, {name: value.name});
                    depositCount.count +=  Number(value.balance);
                    // console.log(depositCount.count,value.name,value.balance);
                  }, 0);
                });

                // Getting total sum for different types of loan accounts
                _.each( loanGroup, (containerTypeAccount) => {
                  _.reduce( containerTypeAccount, (result, value, key) => {
                    // console.log(value);
                    var loanCount = _.find(loanDataSetAccounts, {name: value.name});
                    loanCount.count +=  Number(value.balance);
                  }, 0);
                });

                // Formatting the Account's total values to a more readable string
                depositDataSetAccounts.forEach(account => {
                  account.graph = account.count;
                  account.count = common.transformBalanceToHighDigitNumberWithCurrencySymbol(account.count, "GBP");
                });
                loanDataSetAccounts.forEach(account => {
                  account.graph = account.count;
                  account.count = common.transformBalanceToHighDigitNumberWithCurrencySymbol(account.count, "GBP");
                });

                // console.log(loanDataSetAccounts,'depositDataSetAccounts');


                // Getting data for banks section
                sampsonBankDepositContainer = depositContainer.filter(bank => bank.bankName === "Sampson Bank");
                titanBankDepositContainer = depositContainer.filter(bank => bank.bankName === "ABC Bank");
                sampsonBankLoanContainer = loanContainer.filter(bank => bank.bankName === "Sampson Bank");
                titanBankLoanContainer = loanContainer.filter(bank => bank.bankName === "ABC Bank");

                // Pushing objects to array field
                $scope.dataSet['Summary Account Information']["DEPOSIT"].banks
                  .push(common.buildObjectBankSummary(sampsonBankDepositContainer),common.buildObjectBankSummary(titanBankDepositContainer));

                $scope.dataSet['Summary Account Information']["LOAN"].banks
                  .push(common.buildObjectBankSummary(sampsonBankLoanContainer),common.buildObjectBankSummary(titanBankLoanContainer));


                // the total calculated percentage
                var totalPercentage = 0;

                // // get the last group
                // var lastGroup = $scope.dataSet['Summary Account Information']["DEPOSITS"].reverse()[0];
                // $scope.dataSet['Summary Account Information'].reverse();
                //
                // // iterate the groups to fill in the percentage
                // angular.forEach($scope.dataSet['Summary Account Information']['DEPOSITS'], function (group) {
                //
                //     // calculate the percentage
                //     var percentage = Math.floor((group.count / calculatedTotal) * 100);
                //     // add the calculated percentage to the variable
                //     totalPercentage += percentage;
                //
                //     // if this is the last group, apply correction
                //     if (group === lastGroup) {
                //         // because of Math.floor, totalPercentage will not always be 100%, in order to make it right, a correction is applied to the last group
                //         percentage += 100 - totalPercentage;
                //     }
                //
                //     group.precents = percentage + "%";
                //
                //     // apply formatting to amount
                //     group.count = common.addCurrencySymbolToBalance(common.addThousandsSeparator(group.count, ","), "GBP");
                // });
            });

        });


        //Get data for panel of 'Recommended Actions'
        var promise = common.makeRequest({
            method: 'GET',
            url: 'data/recommended_actions.json'
        });
        promise.then(function (data) {
            $scope.dataSet['Recommended Actions'] = data.dataSet['Recommended Actions'];
        });

        //Get data for panel of 'Tasks'
        // Get all the tasks for the client
        tasksService.getAllTasksForClient(clientId).then(function (response) {

          console.log(response,'response');

            // Map the tasks to objects used in UI
            var mappedTasks = [];
            angular.forEach(response.data, function (value) {
                this.push(new Task(Number(value.taskId), '', value.productCategory, value.type, value.description, value.status, '', value.currency, common.addCurrencySymbolToBalance(common.addThousandsSeparator(value.amount,","), value.currency), '', ''));
            }, mappedTasks);

            // Add mapped objects to the dataset
            $scope.dataSet['Tasks'] = {
                "heading": ["ID", "Account", "Product Category", "Type", "Task Description", "Status", "Counterparty", "Currency", "Amount", "Transactions", "Letter"],
                "matrix": mappedTasks.reverse()
            };

            $scope.dataSet['Tasks'].selectSelected = {
                "text": 6
            };
            $scope.dataSet['Tasks'].selectOptions = [
                { "text": 6  },
                { "text": 12 },
                { "text": 18 },
                { "text": 24 }
            ];
            $scope.currentPage = 1;
            $scope.changePagination = function () {
                $scope.currentPage = 1;
            };

            $scope.changeItemsForSelectedPagination = function() {

              var optionsPerPage = $scope.dataSet['Tasks'].selectSelected.text;
              var currentPage = $scope.currentPage;
              var initial = (currentPage-1)*optionsPerPage;
              var end = initial + optionsPerPage
              console.log($scope.dataSet['Tasks'],$scope.currentPage,initial, end,'$scope.currentPage');
              return mappedTasks.slice(initial,end);
            }
        });


        //Get data for panel of 'Latest News'
        var promise = common.makeRequest({
            method: 'GET',
            url: 'data/latest_news.json'
        });
        promise.then(function (data) {
            $scope.dataSet['Latest News'] = data.dataSet['Latest News'];
        });
        //Modal
        $scope.triggerCurrentAssetsModal = function () {
            $scope.$emit('modal_current_assets', true);
        };
    };

    // declaration of the controller
    var declaration = [
        '$scope',
        'common',
        'tasksService',
        'accountsService',
        'clientsService',
        '$sessionStorage', // faking put request to accounts
        dashboardController
    ];

    // Create the controller
    angular.module("dinnaco").controller("dashboardController", declaration);

})();
