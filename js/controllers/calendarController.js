/// <summary>
/// This is the calendar Controller
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

'use strict';

// self invoking function
(function () {

    // implementation of the calendar controller
    var calendarCtrl = function ($scope, common, moment) {

        // logout clicked
        $scope.logout = function () {
            $scope.mainLogout();
        };

        // check if user is logged in
        $scope.checkForLoggedIn();

        var vm = this;

        //These variables MUST be set as a minimum for the calendar to work
        vm.calendarView = 'month';
        vm.viewDate = new Date();
        vm.events = [];
        vm.lastDateClicked = moment().toDate();
        vm.timespanClicked = function (date) {
            vm.lastDateClicked = date;
        };
        $scope.isToday = function (day) {
            return moment().isSame(day, 'day');
        }
        //Get Events for Calendar
        var promise = common.makeRequest({
            method: 'GET',
            url: 'data/events.json'
        });
        promise.then(function (data) {
            vm.events = data.events;
            angular.forEach(vm.events, function (elem, index) {
                elem.startsAt = moment(elem.startsAt).toDate()
            });
        });
    };

    // declaration of the controller
    var declaration = [
       '$scope',
       'common',
       'moment',
        calendarCtrl
    ];

    // Create the controller
    angular.module('dinnaco').controller("calendarCtrl", declaration);

})();
