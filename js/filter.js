/// <summary>
/// This file contains the filters
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

'use strict';

// self invoking function
(function () {

    var controllerModule = angular.module("dinnaco.controllers");

    controllerModule.filter('int', function () {
        return function (collection) {
            var output;
            if (collection && typeof collection !== 'number') {
                output = parseInt(collection.replace(/[^0-9\.]+/g, ""));
            }
            return output;
        };
    });

    controllerModule.filter('range', function (moment) {
        return function (collection, selected) {
            var output = [];
            if (selected) {
                angular.forEach(collection, function (element, index) {
                    if (moment(selected).isSame(element.startsAt, 'day')) {
                        output.push(element);
                    }
                });
            }
            return output;
        };
    });

    controllerModule.filter('time', function (moment) {
        return function (collection) {
            return moment(collection).format('LT');
        };
    });

    controllerModule.filter('moveZeroToEnd', function () {
        return function (items, field) {
            var sorted = [];
            angular.forEach(items, function (item) {
                sorted.push(item);
            });
            sorted.sort(function (a, b) {
                if (a[field] == 0 && b[field] != 0) {
                    return 1;
                } else if (b[field] == 0 && a[field] != 0) {
                    return -1;
                } else {
                    return 0;
                }
            });
            return sorted;
        };
    });

})();

