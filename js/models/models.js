/// <summary>
/// This file contains the models
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

// The Task model
function Task(id,
                account,
                productCategory,
                type,
                taskDescription,
                status,
                counterparty,
                currency,
                amount,
                transactions,
                letter) {

    this.id = id;
    this.account = account;
    this.productCategory = productCategory;
    this.type = type;
    this.taskDescription = taskDescription;
    this.status = status;
    this.counterparty = counterparty;
    this.currency = currency;
    this.amount = amount;
    this.transactions = transactions;
    this.letter = letter;
}

function modifyAccountNumber(string) {
  var tempString = string.replace(/\B[a-zA-Z]/g, 'X');
  var position = tempString.lastIndexOf('X');
  var firstBitString = tempString.slice(0,position+ 1);
  var lastBitString = tempString.slice(-position);

  return firstBitString.concat('-',lastBitString);
}

// The Account model
function Account(id, name, balance, accountType, bankName) {
    // convert bankImage to a single character so it can be used as an image reference
    var bankNameToLowerCase = string => { return string.toLowerCase().replace(/ /, ''); }
    var defaultBankName = bankName !== "" ? bankName : "Sampson Bank";
    // Generate a random hex color to display as an account reference
    var randomColor = `#${Math.random().toString(16).slice(2, 8).toUpperCase()}`;

    this.id = id == null ? 0 : id;
    this.idModified = modifyAccountNumber(this.id);
    this.name = name;
    this.color = randomColor;
    this.balance = balance == null ? 0 : balance;
    this.accountType = accountType;
    this.bankImage = bankNameToLowerCase(defaultBankName);
    this.bankName = bankName;
}

// The SummaryAccountInfo model
function SummaryAccountInfo(id, productName, balance, percentage) {
    this.key = productName;
    this.value = id;
    this.count = balance;
    this.precents = percentage;
}
