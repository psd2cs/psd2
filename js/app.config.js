﻿/// <summary>
/// This file contains the settings of the app
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

function AppConfig() { };
// This endpoint is commented out because of CORS issues
AppConfig.RestApiEndPoint = 'http://fintech-api.cloudhub.io/api';
// AppConfig.RestApiEndPoint = 'http://mtwomey-test.apigee.net/api';
