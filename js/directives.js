/// <summary>
/// This file contains the directives
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

'use strict';

var appDirectives = angular.module('dinnaco.directives', []);

appDirectives

//Click Off
.directive('clickOff', function($parse, $document) {
	var dir = {
	compile: function($element, attr) {
		// Parse the expression to be executed
		// whenever someone clicks _off_ this element.
		var fn = $parse(attr["clickOff"]);
		return function(scope, element, attr) {
			// add a click handler to the element that
			// stops the event propagation.
			element.bind("touchend", function(event) {
				event.stopPropagation();
			});
			element.bind("click", function(event) {
				event.stopPropagation();
			});
			angular.element($document[0].body).bind("touchend",
				function(event) {
					scope.$apply(function() {
						fn(scope, {$event:event});
					});
				});
			angular.element($document[0].body).bind("click",
				function(event) {
					scope.$apply(function() {
						fn(scope, {$event:event});
					});
				});
			};
		}
	};
	return dir;
})

.directive('range',function($filter){
	return {
		require: 'ngModel',
		link: function(scope, elem, attr, ngModel) {
			var sum = 0,
				maximum;
			scope.$watch(attr.range, function(newArr, oldArr) {
				var valid,
					sum = 0;
				angular.forEach(newArr, function(entity, i) {
					if(entity.value){
						sum += parseFloat(entity.value);
					}
				});
				if(sum !== 0){
					(sum <= $filter('int')(attr.maximum)) ? valid = true : valid = false;
					if(!valid) {
						ngModel.$setValidity('amount', false);
					}else{
						ngModel.$setValidity('amount', true);
						ngModel.$setValidity('range', true);
					}
					ngModel.$render();
				}
			},true);
			//For DOM -> model validation
			ngModel.$parsers.unshift(function(value) {
				var valid,
					_value = $filter('int')(value);
					console.log(_value)
				if(typeof _value !== 'undefined'){
					(_value <= $filter('int')(attr.maximum) && _value <= $filter('int')(attr.balance)) ? valid = true : valid = false;
					ngModel.$setValidity('range', valid);
				}else{
					ngModel.$setValidity('range', true);
				}
				return valid ? value : undefined;
			});

		}
	};
})

.directive('validNumber', function() {
	return {
		require: '?ngModel',
		link: function(scope, element, attrs, ngModelCtrl) {
			if(!ngModelCtrl) {
				return;
			}
			ngModelCtrl.$parsers.push(function(val) {
				if (angular.isUndefined(val)) {
					var val = '';
				}

				var clean = val.replace(/[^-0-9]/g, '');
				var negativeCheck = clean.split('-');
				var decimalCheck = clean.split('.');
				if(!angular.isUndefined(negativeCheck[1])) {
					negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
					clean =negativeCheck[0] + '-' + negativeCheck[1];
					if(negativeCheck[0].length > 0) {
						clean =negativeCheck[0];
					}

				}

				if(!angular.isUndefined(decimalCheck[1])) {
					decimalCheck[1] = decimalCheck[1].slice(0,2);
					clean =decimalCheck[0] + '.' + decimalCheck[1];
				}

				if (val !== clean) {
					ngModelCtrl.$setViewValue(clean);
					ngModelCtrl.$render();
				}
				return clean;
			});

			element.bind('keypress', function(event) {
				if(event.keyCode === 32) {
					event.preventDefault();
				}
			});
		}
	};
})

.directive('stopEvent',function(){
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			if(attr && attr.stopEvent)
			element.bind(attr.stopEvent, function (e) {
				e.stopPropagation();
			});
		}
	};
})

//Triiger Modal
.directive('triggerModal',function($window){
	return {
		restrict: 'AE',
		scope: {
			temple: '@'
		},
		link: function(scope, element, attrs){
			var escapeEvent;
			element.on('click',function(){
				scope.$emit(attrs.temple, true);
				$("html, body").animate({scrollTop:0}, '500', 'swing');
			});
			//we only unbind modal escape and not everything
			escapeEvent = function(e) {
				if (e.which == 27) {
					scope.$emit('modal_close', true);
				}
			};
			jQuery('body').bind('keyup', escapeEvent);
		}
	}
})

.directive('format', function ($filter) {

  return {
    require: '?ngModel',
    restrict: 'A',
    link: function (scope, element, attrs, ngModel) {

      if (!ngModel) return;

	  ngModel.$formatters.unshift(function (a) {
		  return $filter('number')(ngModel.$modelValue);
	  });

	  ngModel.$parsers.unshift(function (viewValue) {
		  var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
		  element.val($filter('number')(plainNumber));
		  return plainNumber;
	  });

    }
  }

})

//Common Header
.directive('commonHeader', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Header.html'
	};
})

//Common Footer
.directive('commonFooter', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Footer.html'
	};
})

// Summary Information panel
.directive('summaryInfoPanel', function() {
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Summary_Information.html'
	}
})

//Modal For Confirm Removal
.directive('modalComfirmRemoval', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Modal_Comfirm_Removal.html'
	};
})

//Modal For New Letter of Credit Created
.directive('modalCreditCreated', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Modal_Credit_Created.html'
	};
})

//Modal For Current Assets
.directive('modalCurrentAssets', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Modal_Current_Assets.html',
		controller: 'currentAssetsController'
	};
})

//Modal For Current Assets
.directive('modalSummaryAccountInformation', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Modal_Summary_Account_Information.html',
		controller: 'summaryAccountInfomrationController'
	};
})

//Modal For Assets Liabilities
.directive('modalAssetsLiabilities', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Modal_Assets_Liabilities.html'
	};
})

//Modal For Recommended Actions
.directive('modalRecommendedActions', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Modal_Recommended_Actions.html',
		controller: 'recommendedActionsController'
	};
})

//Modal For Tasks
.directive('modalTasks', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Modal_Tasks.html',
		controller: 'tasksController'
	};
})

//Modal For Calendar
.directive('modalCalendar', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Modal_Calendar.html'
	};
})

//Modal For Rates
.directive('modalRates', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Modal_Rates.html'
	};
})

//Modal For Latest News
.directive('modalLatestNews', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Modal_Latest_News.html',
		controller: 'latestNewsController'
	};
})

//Modal For Outstanding Payables
.directive('modalOutstandingPayables', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Modal_Outstanding_Payables.html'
	};
})

//Modal for Trade - Investment by Product
.directive('modalTradeInvestment', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Modal_Trade_Investment.html'
	};
})

//Modal for Import Letter of Credit
.directive('modalImportLetterCredit', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Modal_Import_Letter_Credit.html'
	};
})

//Modal for Trade Tasks
.directive('modalTradeTasks', function(){
	return {
		restrict: 'AE',
		templateUrl: 'templetes/Modal_Trade_Tasks.html',
		controller: 'tasksController'
	};
});
