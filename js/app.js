/// <summary>
/// This is the starting point of the angular app
/// </summary>
/// <version>1.0</version>
/// <copyright>Copyright (c) 2016, TopCoder, Inc. All rights reserved.</copyright>

'use strict';

// self invoking function
(function () {

    angular.module('dinnaco.controllers', []);
    angular.module('dinnaco.directives', []);
    angular.module('dinnaco.services', []);
    angular.module('dinnaco.filter', []);

    var app = angular.module('dinnaco',
                        ['ngRoute',
                        'ngSanitize',
                        'ngDropdowns',
                        'ngStorage',
                        'ui.bootstrap',
                        'ngMessages',
                        'flow',
                        'mwl.calendar',
                        'angular.backtop',
                        'bcherny/formatAsCurrency',
                        'dinnaco.controllers',
                        'dinnaco.directives',
                        'dinnaco.services',
                        'dinnaco.filter']);

    app.config(["$routeProvider", "$locationProvider",
    function ($routeProvider) {
        $routeProvider
			.when("/", {
			    templateUrl: "partials/Login.html",
			    controller: 'loginController'
			})
			.when("/Dashboard", {
			    templateUrl: "partials/Dashboard.html",
			    controller: 'dashboardController'
			})
			.when("/Trade", {
			    templateUrl: "partials/Trade.html",
			    controller: 'tradeController'
			})
			.when("/Letters_Of_Credit", {
			    templateUrl: "partials/Letters_of_Credit.html",
			    controller: 'lettersOfCreditController'
			})
			.when("/New_Letter_Of_Credit", {
			    templateUrl: "partials/New_Letter_of_Credit.html",
			    controller: 'newLetterOfCreditController'
			});
    }
    ]);

    // Add the config constant
    app.constant('CONFIG', {
        restApiEndPoint: AppConfig.RestApiEndPoint,
    });

    // Initialize the main module
    app.run(['$rootScope', '$location', '$window', function ($rootScope, $location, $window) {
        $rootScope.location = $location;
        $rootScope.goto = function (path) {
            $rootScope.isExpand = false;
            if (path === 'back') { // Allow a 'back' keyword to go to previous page
                $window.history.back();
            } else { // Go to the specified path
                $location.path(path);
            }
        };
    }]);
})();
